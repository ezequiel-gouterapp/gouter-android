package security.gouter;

import android.app.Application;

import security.gouter.Dagger.ObjectGraph;

/**
 * Created by ezequiel on 29/09/15.
 */
public class GouterApplication extends Application {

	// SharedPreferences constants
	public static final String PREFERENCES_ACCOUNT = "accountPreferences";
	public static final String PREFERENCES_USER = "userData";

    // APIS
    //	private static final String SERVER = "http://www.gouterapp.com";
    public static final String API_USER = "%1$s/api/users/";
    public static final String API_INVITATIONS = "%1$s/api/users/%2$s/invitations";

	// Local storage constants
	public static final String PROFILE_IMAGES = "profileImages";

	private static ObjectGraph objectGraph;

	@Override
	public void onCreate() {
		super.onCreate();
		objectGraph = ObjectGraph.Initializer.init(this);
	}

	public static ObjectGraph getObjectGraph() {
		return objectGraph;
	}


}
