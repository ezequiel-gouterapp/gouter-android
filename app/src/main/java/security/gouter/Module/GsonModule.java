package security.gouter.Module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ezequiel on 17/10/15.
 */
@Module
public class GsonModule {

	@Provides
	Gson provideGson() {
		final GsonBuilder gsonBuilder = new GsonBuilder();
		return gsonBuilder.create();
	}
}
