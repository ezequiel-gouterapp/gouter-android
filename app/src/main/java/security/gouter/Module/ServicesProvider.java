package security.gouter.Module;

import dagger.Module;
import dagger.Provides;
import security.gouter.Service.GouterService;
import security.gouter.Service.GouterServiceImpl;
import security.gouter.Service.UserService;
import security.gouter.Service.UserServiceImpl;

/**
 * Created by ezequiel on 30/09/15.
 */

@Module
public class ServicesProvider {

	@Provides
	UserService provideUserService(final UserServiceImpl userService) {
		return userService;
	}

	@Provides
	GouterService proGouterService(final GouterServiceImpl gouterService) {
		return gouterService;
	}
}
