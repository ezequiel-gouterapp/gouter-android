package security.gouter.Module;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ezequiel on 17/10/15.
 */

@Module
public class VolleyModule {

	@Provides
	RequestQueue provideRequestQueue(final Context context) {
		return Volley.newRequestQueue(context);
	}
}
