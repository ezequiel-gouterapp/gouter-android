package security.gouter.Module;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ezequiel on 29/09/15.
 */

@Module(includes = {ServicesProvider.class, VolleyModule.class,
		GsonModule.class})
public class MainModule {

	final Application application;

	public MainModule(final Application application) {
		this.application = application;
	}

	@Provides
	Application provideApplication() {
		return this.application;
	}

	@Provides
	Context provideContext() {
		return this.application.getApplicationContext();
	}

}
