package security.gouter.Dagger;

import security.gouter.Activity.HomeActivity;
import security.gouter.Activity.MainActivity;
import security.gouter.Adapter.Holders.EventViewHolderImpl;
import security.gouter.Util.UserUtils;

/**
 * Created by ezequiel on 29/09/15.
 */
public interface Graph {

	/**
	 * @param mainActivity Injected activity
	 */
	void inject(MainActivity mainActivity);

	/**
	 * @param homeActivity Injected Activity
	 */
	void inject(HomeActivity homeActivity);

	/**
	 * @param userUtils Injected by Dagger
	 */
	void inject(UserUtils userUtils);

    /**
     * @param eventViewHolderImpl Injected by Dagger
     */
    void inject(EventViewHolderImpl eventViewHolderImpl);
}
