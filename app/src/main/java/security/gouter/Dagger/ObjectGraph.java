package security.gouter.Dagger;

import android.app.Application;

import dagger.Component;
import security.gouter.Module.MainModule;

/**
 * Created by ezequiel on 29/09/15.
 */
@Component(modules = MainModule.class)
public interface ObjectGraph extends Graph {
	final class Initializer {
		private Initializer() {

		}

		public static ObjectGraph init(final Application application) {
			return DaggerObjectGraph.builder()
					.mainModule(new MainModule(application))
					.build();
		}
	}
}
