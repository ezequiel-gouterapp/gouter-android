package security.gouter.Adapter.Fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by ezequiel on 16/11/15.
 */
public class EventDetailAdapter extends FragmentPagerAdapter {

    private final ArrayList<Fragment> fragments;

    public EventDetailAdapter(final FragmentManager fm, final ArrayList<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(final int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
