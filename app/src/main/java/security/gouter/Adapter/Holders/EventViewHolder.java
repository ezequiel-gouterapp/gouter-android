package security.gouter.Adapter.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import security.gouter.Model.Event;

/**
 * Created by ezequiel on 27/10/15.
 */
public abstract class EventViewHolder extends RecyclerView.ViewHolder {

	public EventViewHolder(View itemView) {
		super(itemView);
	}

	public abstract void onBind(Event event);
}
