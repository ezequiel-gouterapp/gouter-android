package security.gouter.Adapter.Holders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import security.gouter.Activity.EventDetailActivity;
import security.gouter.GouterApplication;
import security.gouter.Model.Event;
import security.gouter.R;
import security.gouter.Service.UserService;
import security.gouter.UiComponents.RoundedImageView;

/**
 * Created by ezequiel on 27/10/15.
 */
public class EventViewHolderImpl extends EventViewHolder {

    private Event event;
    private Context context;

    private static final int IMAGE_SIZE = 600;

    @Inject
    /* default */ UserService userService;

    @Bind(R.id.event_cover)
    /* default */ ImageView eventCover;

    @Bind(R.id.author_image)
    /* default */ RoundedImageView authorImage;

    @Bind(R.id.event_title)
    /* default */ TextView eventTitle;

	public EventViewHolderImpl(final View itemView, final Activity context) {
		super(itemView);

        this.context = context;

        ButterKnife.bind(this, itemView);
        GouterApplication.getObjectGraph().inject(this);

        // CardView configuration
		final CardView cardView = (CardView) itemView;
		cardView.setCardBackgroundColor(context.getResources().getColor(R.color.white));
		cardView.setShadowPadding(5, 5, 5, 5);
		cardView.setRadius(5);

		// Click listener
		itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View v) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
					final ImageView cover = (ImageView) v.findViewById(R.id.event_cover);
					final Intent intent = new Intent(context, EventDetailActivity.class);
                    intent.putExtra(EventDetailActivity.EVENT, event);
					final ActivityOptionsCompat options = ActivityOptionsCompat
							.makeSceneTransitionAnimation(context,
									new Pair<View, String>(cover, "cardViewCover"));
					ActivityCompat.startActivity(context, intent, options.toBundle());
				} else {
					final Intent intent = new Intent(context, EventDetailActivity.class);
					intent.putExtra(EventDetailActivity.EVENT, event);
					context.startActivity(intent);
				}
			}
		});
	}


	@Override
	public void onBind(final Event event) {
        this.event = event;

        // Adjust rounded image position
        final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                authorImage.getLayoutParams();
        layoutParams.setMargins(0, (int) context.getResources().getDimension(R.dimen.event_cover)
                - (int) context
                .getResources().getDimension(R.dimen.event_detail_profile_picture) / 2, 0, 0);
        // Author image position
        authorImage.setLayoutParams(layoutParams);
        // Author image request
        Bitmap bitmap = event.getAuthor().getProfileImage(context);

        if (bitmap == null) {
            userService.getUserImage(event.getAuthor(), IMAGE_SIZE, IMAGE_SIZE, new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(final Bitmap bitmap) {
                    authorImage.setImageBitmap(bitmap);
                    event.getAuthor().setProfileImage(context, bitmap);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError volleyError) {
                    Toast.makeText(context, context.getResources().getString(R.string.feedback_image_error),
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            authorImage.setImageBitmap(event.getAuthor().getProfileImage(context));
        }

        // Event title
        eventTitle.setText(event.getTitle());
	}
}
