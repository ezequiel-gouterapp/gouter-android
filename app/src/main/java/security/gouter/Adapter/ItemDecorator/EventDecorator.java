package security.gouter.Adapter.ItemDecorator;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import security.gouter.R;

/**
 * Created by ezequiel on 27/10/15.
 */
public class EventDecorator extends RecyclerView.ItemDecoration {

	private final Context context;

	public EventDecorator(final Context context) {
		this.context = context;
	}

	@Override
	public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
		super.getItemOffsets(outRect, view, parent, state);
		final int padding = (int) context.getResources().getDimension(R.dimen.event_item_padding);
		outRect.set(0, padding, 0, padding);
	}
}
