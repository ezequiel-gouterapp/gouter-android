package security.gouter.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import security.gouter.Adapter.Holders.EventViewHolder;
import security.gouter.Adapter.Holders.EventViewHolderImpl;
import security.gouter.Model.Event;
import security.gouter.R;

/**
 * Created by ezequiel on 27/10/15.
 */
public class EventsAdapter extends RecyclerView.Adapter<EventViewHolder> {

	private final ArrayList<Event> events;
	private final Activity context;

	public EventsAdapter(final ArrayList<Event> events, final Activity context) {
		this.events = events;
		this.context = context;
	}

	@Override
	public EventViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
		final LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View view = inflater.inflate(R.layout.item_event, null);

		return new EventViewHolderImpl(view, context);
	}

	@Override
	public void onBindViewHolder(final EventViewHolder holder, final int position) {
		holder.onBind(events.get(position));
	}

	@Override
	public int getItemCount() {
		return events.size();
	}
}
