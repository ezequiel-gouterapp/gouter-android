package security.gouter.Listeners;

import android.location.Location;

/**
 * Created by ezequiel on 08/10/15.
 */
public interface LocationListener {
	void onLocationUpdate(Location location);
}
