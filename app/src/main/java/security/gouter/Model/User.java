package security.gouter.Model;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import security.gouter.Util.UserUtils;

/**
 * Created by ezequiel on 17/10/15.
 */
public class User implements Serializable {

	private transient Bitmap profileImage;

    @SerializedName("id")
	private String id;
    @SerializedName("name")
	private String name;
    @SerializedName("email")
	private String email;
    @SerializedName("facebook_id")
    private long fbId;
    @SerializedName("gender")
	private String gender;
    @SerializedName("regid")
	private String regId;
    @SerializedName("image")
	private String imageUrl;

    /**
     * Empty Cosntructor
     */
    public User() {}

    /**
     * Returns user image from local storage
     * @param context the context
     * @return The image
     */
	public Bitmap getProfileImage(final Context context) {
		if (profileImage == null) {
			profileImage = UserUtils.getProfileImageFromStorage(context, getId());
		}

		return profileImage;
	}

	public void setProfileImage(final Context context, final Bitmap profileImage) {
		this.profileImage = profileImage;
        UserUtils.saveToInternalSorage(context, profileImage, getId());
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(final String gender) {
		this.gender = gender;
	}

	public String getRegId() {
		return regId;
	}

	public void setRegId(final String regId) {
		this.regId = regId;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(final String imageUrl) {
		this.imageUrl = imageUrl;
	}

    public long getFbId() {
        return fbId;
    }

    public void setFbId(final long fbId) {
        this.fbId = fbId;
    }
}
