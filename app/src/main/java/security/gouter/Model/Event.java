package security.gouter.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ezequiel on 27/10/15.
 */
public class Event implements Serializable {

    @SerializedName("id")
    private
	/* default */ String id;

    @SerializedName("title")
    private
	/* default */ String title;

    @SerializedName("description")
    private
	/* default */ String description;

    @SerializedName("access")
    private
	/* default */ String access;

    @SerializedName("address")
    private
	/* default */ String address;

    @SerializedName("dateTime")
    /* default */ String dateTime;

    @SerializedName("author")
    private
	/* default */ User author;

    @SerializedName("guests")
    private
	/* default */ ArrayList<User> invitedUsers;

    public Event() {

    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getAccess() {
        return access;
    }

    public String getAddress() {
        return address;
    }

    public String getDateTime() {
        return dateTime;
    }

    public User getAuthor() {
        return author;
    }

    public ArrayList<User> getInvitedUsers() {
        return invitedUsers;
    }
}
