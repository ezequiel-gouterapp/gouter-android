package security.gouter.Activity;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import butterknife.ButterKnife;
import security.gouter.R;
import security.gouter.Service.UserService;
import security.gouter.Util.UiUtils;
import security.gouter.Util.UserUtils;

public class ToolbaredActivity extends AppCompatActivity {

    private static final int IMAGE_SIZE = 600;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbared);
    }

    protected void initializeActionBar(final UserService userService) {
        final Toolbar toolbar = ButterKnife.findById(this, R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(" " + userService.getLoggedUser().getName());

        // Download image if necessary, if not we get it from storage
        final int dimension = (int) getResources().getDimension(R.dimen.actionbarImage);
        if (userService.getLoggedUser().getProfileImage(
                getApplicationContext()) == null) {
            userService.getUserImage(userService.getLoggedUser(), IMAGE_SIZE, IMAGE_SIZE,
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(final Bitmap bitmap) {
                            userService.getLoggedUser().setProfileImage(getApplicationContext(), bitmap);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(final VolleyError volleyError) {

                        }
                    });
        } else {
            /**
             * We get image from local storage
             */
            final BitmapDrawable drawable = new BitmapDrawable(getResources(),
                    UiUtils.createCircleBitmap(userService.getLoggedUser().getProfileImage(this)));
            getSupportActionBar().setIcon(drawable);
        }

        // Status bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.primary_color));
        } else {
            final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.root);
            coordinatorLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.primary_color));
        }
    }

}
