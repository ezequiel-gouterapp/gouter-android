package security.gouter.Activity;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import security.gouter.Adapter.Fragment.EventDetailAdapter;
import security.gouter.Fragments.EventDetailGallery;
import security.gouter.Fragments.EventDetailInfo;
import security.gouter.Fragments.EventDetailPeople;
import security.gouter.Model.Event;
import security.gouter.R;

public class EventDetailActivity extends AppCompatActivity {

    public static String EVENT = "event";

    private Event event;

    @Bind(R.id.viewPager)
    /* default */ ViewPager viewPager;

    @Bind(R.id.tabLayout)
    /* default */ TabLayout tabLayout;

    private EventDetailAdapter eventDetailAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_detail);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

        // We get data
        event = (Event) getIntent().getSerializableExtra(EVENT);

        // ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setTitle(event.getTitle());

		// Status bar
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			getWindow().setStatusBarColor(getResources().getColor(R.color.primary_color));
		} else {
			final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.root);
			coordinatorLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.primary_color));
		}

        initializeTabs();
	}

    private void initializeTabs() {
        final ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(EventDetailInfo.newInstance(event));
        fragments.add(new EventDetailPeople());
        fragments.add(new EventDetailGallery());

        eventDetailAdapter = new EventDetailAdapter(getSupportFragmentManager(), fragments);

        tabLayout.addTab(tabLayout.newTab().setText("Info"));
        tabLayout.addTab(tabLayout.newTab().setText("Invited"));
        tabLayout.addTab(tabLayout.newTab().setText("Gallery"));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(final TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(final TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(final TabLayout.Tab tab) {

            }
        });
        viewPager.setAdapter(eventDetailAdapter);
    }
}
