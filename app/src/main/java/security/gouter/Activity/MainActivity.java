package security.gouter.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import javax.inject.Inject;

import security.gouter.GouterApplication;
import security.gouter.Model.User;
import security.gouter.R;
import security.gouter.Service.GouterService;
import security.gouter.Service.UserService;
import security.gouter.Util.UserUtils;

public class MainActivity extends AppCompatActivity {

	@Inject
	UserService userService;

	@Inject
	GouterService gouterService;

	@Inject
	Gson gson;

	private CallbackManager callbackManager;

	private User user = new User();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FacebookSdk.sdkInitialize(this);
		setContentView(R.layout.activity_main);

		GouterApplication.getObjectGraph().inject(this);

		final LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
		loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_friends"));

		callbackManager = CallbackManager.Factory.create();

		final Activity activity = this;

		loginButton.registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {
					@Override
					public void onSuccess(final LoginResult loginResult) {
						GraphRequest request = GraphRequest.newMeRequest(
								loginResult.getAccessToken(),
								new GraphRequest.GraphJSONObjectCallback() {
									@Override
									public void onCompleted(final JSONObject jsonObject,
									                        final GraphResponse graphResponse) {
										try{
											user.setEmail(jsonObject.getString("email"));
											user.setName(jsonObject.getString("name"));
											user.setGender(jsonObject.getString("gender"));
											user.setFbId(jsonObject.getLong("id"));
                                            user.setImageUrl(UserUtils.getPorfileImageUri());
											user.setRegId("1234");
											gouterService.registerUser(user,
													new Response.Listener<User>() {
														@Override
														public void onResponse(final User user) {
															/**
															 * Saving user
															 */
															userService.onUpdate(user);

															final Intent intent = new Intent(activity, HomeActivity.class);
															startActivity(intent);
															activity.finish();
														}
													},
													new Response.ErrorListener() {
														@Override
														public void onErrorResponse(VolleyError volleyError) {
															Toast.makeText(getApplicationContext(),
																	getResources().getString(R.string.error_register),
																	Toast.LENGTH_LONG).show();
														}
													});
										} catch(final JSONException e) {

										}
									}
								}
						);
						Bundle parameters = new Bundle();
						parameters.putString("fields", "id,name,email,gender, birthday");
						request.setParameters(parameters);
						request.executeAsync();
					}

					@Override
					public void onCancel() {
						Log.d("email", "canceled");
					}

					@Override
					public void onError(FacebookException e) {
						Log.d("email", "error");
					}
				});

		if (userService.getProfile() != null) {
			if (userService.getLoggedUser() == null) {
				userService.logout(this);
			} else {
				final Intent intent = new Intent(this, HomeActivity.class);
				startActivity(intent);
				this.finish();
			}
		}

		userService.login(this);
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}
}
