package security.gouter.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import javax.inject.Inject;

import security.gouter.Adapter.EventsAdapter;
import security.gouter.Adapter.ItemDecorator.EventDecorator;
import security.gouter.GouterApplication;
import security.gouter.Model.Event;
import security.gouter.R;
import security.gouter.Service.GouterService;
import security.gouter.Service.UserService;
import security.gouter.Util.UiUtils;

public class HomeActivity extends ToolbaredActivity {

    private boolean isMenuShown = false;
	private View.OnClickListener onClickListener;

	@Inject
	UserService userService;

    @Inject
    GouterService gouterService;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		GouterApplication.getObjectGraph().inject(this);


		if (userService.getLoggedUser() == null) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			this.finish();
		} else {
			initializeActionBar(userService);
		}

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabButton);

        // Adding mini fab buttons
        final FloatingActionButton miniFabSettings = (FloatingActionButton) findViewById(R.id.fabSettings);
        final FloatingActionButton miniFabCreateEvent = (FloatingActionButton) findViewById(R.id.fabCreateEvent);
        final FloatingActionButton[] miniFabs = new FloatingActionButton[2];
        miniFabs[0] = miniFabSettings;
        miniFabs[1] = miniFabCreateEvent;

        final RelativeLayout cap = (RelativeLayout) findViewById(R.id.cap);

		// Black Background click
		cap.setOnClickListener(getMenuListener(fab, miniFabs, cap));;

		// Fab Menu
        fab.setOnClickListener(getMenuListener(fab, miniFabs, cap));

        // We make the events request
        final Activity activity = this;
        userService.getInvitations(userService.getLoggedUser(), new Response.Listener<ArrayList<Event>>() {
                    @Override
                    public void onResponse(final ArrayList<Event> events) {
                        final RecyclerView listView = (RecyclerView) findViewById(R.id.recyclerView);
                        listView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        listView.addItemDecoration(new EventDecorator(getApplicationContext()));
                        listView.setAdapter(new EventsAdapter(events, activity));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError volleyError) {
                        Toast.makeText(getApplicationContext(), R.string.feedback_events_error, Toast.LENGTH_LONG)
                                .show();
                    }
                });
    }

	private View.OnClickListener getMenuListener(final FloatingActionButton fab,
	                                             final FloatingActionButton[] miniFabs, final View cap) {
		if (onClickListener == null) {
			onClickListener = new View.OnClickListener() {
				@Override
				public void onClick(final View v) {
					if (isMenuShown) {
						UiUtils.hideFabMenu(getApplicationContext(), fab, miniFabs, cap);
					} else {
						UiUtils.showFabMenu(getApplicationContext(), fab, miniFabs, cap);
					}

					isMenuShown = !isMenuShown;
				}
			};
		}

		return onClickListener;
	}

	private void launchCreateEvent(final View view) {
		// TODO : Create Events Activity
	}

	private void launchSettings(final View view) {
		// TODO : Create Settings Activity
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_home, menu);
		return true;
	}
}
