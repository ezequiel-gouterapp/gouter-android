package security.gouter.Service;

import com.android.volley.AuthFailureError;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.google.gson.Gson;
import com.monits.volleyrequests.network.request.GsonRequest;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import security.gouter.GouterApplication;
import security.gouter.Model.User;
import security.gouter.network.request.UrlGsonRequest;

/**
 * Created by ezequiel on 17/10/15.
 */
public class GouterServiceImpl implements GouterService {

	private final RequestQueue requestQueue;
	private final Gson gson;

    private static final String SERVER = "http://192.168.0.16:3100";

	@Inject
	public GouterServiceImpl(final RequestQueue requestQueue,
	                         final Gson gson) {
		this.requestQueue = requestQueue;
		this.gson = gson;
	}

	@Override
	public void getUserById(final int userId, final Response.Listener<User> responseListener,
	                        final Response.ErrorListener errorListener) {
		final GsonRequest<User> userGsonRequest
				= new GsonRequest<User>(Request.Method.POST, new StringBuilder()
		.append(getHost()).append(GouterApplication.API_USER).toString(),
				gson, User.class, responseListener, errorListener, null);

        addRequest(userGsonRequest);
	}

	@Override
	public void registerUser(final User user, final Response.Listener<User> responseListener,
	                         final Response.ErrorListener errorListener) {
        final String url = String.format(Locale.US, GouterApplication.API_USER, getHost());

		final UrlGsonRequest<User> userGsonRequest = new UrlGsonRequest<User>(
				Request.Method.POST, url,
				User.class, gson, responseListener, errorListener) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                final HashMap<String, String> params = new HashMap<>();

                params.put("name", user.getName());
                params.put("email", user.getEmail());
                params.put("image", user.getImageUrl());
                params.put("regid", user.getRegId());
                params.put("facebook_id", String.valueOf(user.getFbId()));
                params.put("gender", user.getGender());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                final Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }
        };
        addRequest(userGsonRequest);
	}

    @Override
    public void addRequest(Request<?> request) {
        requestQueue.add(request);
    }

    @Override
    public String getHost() {
        return SERVER;
    }
}
