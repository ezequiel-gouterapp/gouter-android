package security.gouter.Service;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.Response;
import com.facebook.Profile;

import java.util.ArrayList;

import security.gouter.Model.Event;
import security.gouter.Model.User;

/**
 * Created by ezequiel on 29/09/15.
 */
public interface UserService {

	void login(Context context);

	void logout(Context context);

	Profile getProfile();

	/**
	 * @param userId
	 */
	void getUserById(int userId, Response.Listener<User> responseListener,
	             Response.ErrorListener errorListener);

	User getLoggedUser();

	void onUpdate(User user);

    /**
     * @param user we want to get invitations from this user
     * @param listener Callback if request was successful
     * @param error Callback if request failed
     */
    void getInvitations(User user, Response.Listener<ArrayList<Event>> listener,
                        Response.ErrorListener error);

    /**
     * Downloads image from url
     * @param user user
     * @param width image width
     * @param height image height
     * @param listener Callback if request was successful
     * @param errorListener Callback if request failed
     */
    void getUserImage(User user, int width, int height, Response.Listener<Bitmap> listener,
                      Response.ErrorListener errorListener);
}
