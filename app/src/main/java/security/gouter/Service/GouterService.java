package security.gouter.Service;

import com.android.volley.Request;
import com.android.volley.Response;

import security.gouter.Model.User;

/**
 * Created by ezequiel on 17/10/15.
 */
public interface GouterService {

    String getHost();

	/**
	 * @param userId
	 * @return
	 */
	void getUserById(int userId, Response.Listener<User> responseListener,
	                 Response.ErrorListener errorListener);

	/**
	 * @param user
	 * @param responseListener
	 * @param errorListener
	 */
	void registerUser(User user, Response.Listener<User> responseListener,
	                  Response.ErrorListener errorListener);

    void addRequest(Request<?> requestQueue);
}
