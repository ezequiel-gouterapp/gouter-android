package security.gouter.Service;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.monits.volleyrequests.network.request.GsonRequest;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Locale;

import javax.inject.Inject;

import security.gouter.GouterApplication;
import security.gouter.Model.Event;
import security.gouter.Model.User;
import security.gouter.Util.UserUtils;

/**
 * Created by ezequiel on 29/09/15.
 */
public class UserServiceImpl implements UserService {

	private ProfileTracker profileTracker;
	private GouterService gouterService;
	private Gson gson;
	private Context context;
	private User user;

	@Inject
	public UserServiceImpl(final Context context, final Gson gson,
	                       final GouterService gouterService) {
		this.gouterService = gouterService;
		this.context = context;
		this.gson = gson;
	}

	@Override
	public void login(final Context context) {
		profileTracker = new ProfileTracker() {
			@Override
			protected void onCurrentProfileChanged(
					Profile oldProfile,
					Profile currentProfile) {
				if (currentProfile == null) {
					logout(context);
				}
			}
		};

		profileTracker.startTracking();
	}

	@Override
	public void logout(final Context context) {
		LoginManager.getInstance().logOut();
		UserUtils.resetPreferences(context);
	}

	@Override
	public Profile getProfile() {
		return Profile.getCurrentProfile();
	}

	@Override
	public void getUserById(final int userId, final Response.Listener<User> responseListener,
	                    final Response.ErrorListener errorListener) {
		gouterService.getUserById(userId, responseListener, errorListener);
	}

	@Override
	public User getLoggedUser() {
		if(user == null){
			user = UserUtils
					.getUserFromPreferences(context, gson);
		}

		return user;
	}

	@Override
	public void onUpdate(final User user) {
		UserUtils.saveUserOnpreferences(context,
				gson.toJson(user));
		this.user = user;
	}

    @Override
    public void getInvitations(final User user, final Response.Listener<ArrayList<Event>> listener,
                               final Response.ErrorListener error) {
        final String url = String.format(Locale.US, GouterApplication.API_INVITATIONS,
                gouterService.getHost(), user.getId());
        final Type listType = new TypeToken<ArrayList<Event>>() { }.getType();
        final GsonRequest<ArrayList<Event>> gsonRequest = new GsonRequest<ArrayList<Event>>(
                Request.Method.GET, url, gson, listType, listener, error, null);

        gouterService.addRequest(gsonRequest);
    }

    @Override
    public void getUserImage(final User user, final int width, final int height,
                               final Response.Listener<Bitmap> listener, final Response.ErrorListener errorListener) {
        final ImageRequest request = new ImageRequest(user.getImageUrl(), listener, width, height, null, errorListener);
        gouterService.addRequest(request);
    }
}
