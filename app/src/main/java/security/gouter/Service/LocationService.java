package security.gouter.Service;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.maps.LocationSource;

import security.gouter.Listeners.LocationListener;

/**
 * Created by ezequiel on 06/10/15.
 */
public class LocationService implements LocationSource.OnLocationChangedListener,
		android.location.LocationListener {

	private LocationManager locationManager;
	private Location location;
	private static LocationService instance;
	private LocationListener locationListener;

	private LocationService(final Context context,
							final LocationListener locationListener) {
		this.locationListener = locationListener;
		locationManager = (LocationManager) context
				.getSystemService(Activity.LOCATION_SERVICE);
	}

	public void requestUpdate() {
		String provider = null;
		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			provider = LocationManager.GPS_PROVIDER;
		} else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			provider = LocationManager.NETWORK_PROVIDER;
		}

		locationManager.requestSingleUpdate(provider, this, null);
	}
	public static LocationService getInstance(final Context ctx,
                                  LocationListener locationListener){
		if (instance == null) {
			instance = new LocationService(ctx, locationListener);
		}

		return instance;
	}

	@Override
	public void onLocationChanged(final Location location) {
		this.location = location;
		locationListener.onLocationUpdate(location);
	}

	@Override
	public void onStatusChanged(String s, int i, Bundle bundle) {

	}

	@Override
	public void onProviderEnabled(String s) {

	}

	@Override
	public void onProviderDisabled(String s) {

	}
}
