package security.gouter.network.request;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.monits.volleyrequests.network.request.RfcCompliantListenableRequest;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

/**
 * Created by ezequiel on 14/11/15.
 */
public class UrlGsonRequest<T> extends RfcCompliantListenableRequest {

    private final Type clazz;
    private final Gson gson;

    public UrlGsonRequest(final int method, final String url, final Type clazz, final Gson gson,
                          final Response.Listener listener, final Response.ErrorListener errListener) {
        super(method, url, listener, errListener);
        this.clazz = clazz;
        this.gson = gson;
    }

    @Override
    protected Response<T> parseNetworkResponse(final NetworkResponse response) {
        try {
            final String json = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success((T) gson.fromJson(json, clazz),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (final UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (final JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }
}
