package security.gouter.Util;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import security.gouter.Model.User;
import security.gouter.R;

/**
 * Created by ezequiel on 08/10/15.
 */
public class UiUtils {

    private static final int OFFSET = 125;
    private static final int FADE_DURATION = 250;
	private static final int DURATION = 300;
	private static final float DEGREE = 135;

	/**
	 * @param context our Activity context
	 * @param view to inflate
	 * @return
	 */
	public static Bitmap createDrawableFromView(final Activity context,
	                                            final View view, final User user) {
		final DisplayMetrics displayMetrics = new DisplayMetrics();
		final ImageView imageView = (ImageView) view.findViewById(R.id.marker_profile);

		imageView.setImageBitmap(user.getProfileImage(context));
		context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		view.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT));
		view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.buildDrawingCache();
		final Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
		final Canvas canvas = new Canvas(bitmap);
		view.draw(canvas);

		return bitmap;
	}

	public static Bitmap createCircleBitmap(Bitmap bitmapimg){
		Bitmap output = Bitmap.createBitmap(bitmapimg.getWidth(),
				bitmapimg.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmapimg.getWidth(),
				bitmapimg.getHeight());

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawCircle(bitmapimg.getWidth() / 2,
                bitmapimg.getHeight() / 2, bitmapimg.getWidth() / 2, paint);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmapimg, rect, rect, paint);
		return output;
	}

	public static void hideFabMenu(final Context context, final FloatingActionButton fab,
	                               final FloatingActionButton[] miniFabs, final View cap) {


		ViewCompat.animate(fab).rotation(0).setInterpolator(new OvershootInterpolator())
				.setDuration(DURATION).setListener(new ViewPropertyAnimatorListener() {
			@Override
			public void onAnimationStart(final View view) {
				// Nothing to do here
			}

			@Override
			public void onAnimationEnd(final View view) {
				cap.setVisibility(View.GONE);
			}

			@Override
			public void onAnimationCancel(final View view) {
				// Nothing to do here
			}
		}).start();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			final int margin = (int) context.getResources().getDimension(R.dimen.fab_margin);
			int initialRadius, finalRadius;

			initialRadius = cap.getHeight();
			finalRadius = 0;

			final Animator animator = ViewAnimationUtils.createCircularReveal(cap,
					(cap.getWidth() - (fab.getWidth() / 2) - margin),
					(cap.getHeight() - (fab.getHeight() / 2) - margin),
					initialRadius, finalRadius);
			animator.start();

		}

		for (int i = 0; i < miniFabs.length; i++) {
			final int position = i;
			final Animation anim = AnimationUtils.loadAnimation(context, R.anim.abc_fade_out);
			anim.setDuration(FADE_DURATION);
			anim.setAnimationListener(new Animation.AnimationListener() {
				@Override
				public void onAnimationStart(final Animation animation) {

				}

				@Override
				public void onAnimationEnd(final Animation animation) {
					miniFabs[position].setVisibility(View.GONE);
				}

				@Override
				public void onAnimationRepeat(final Animation animation) {

				}
			});
			miniFabs[i].setVisibility(View.VISIBLE);
			anim.setStartOffset((miniFabs.length - i) * OFFSET);
			miniFabs[i].startAnimation(anim);
		}
	}

    public static void showFabMenu(final Context context, final FloatingActionButton fab,
                                   final FloatingActionButton[] miniFabs, final View cap) {

	    ViewCompat.animate(fab).rotation(DEGREE).setInterpolator(new OvershootInterpolator())
			    .setDuration(DURATION).setListener(null).start();

	    cap.setVisibility(View.VISIBLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
	        final int margin = (int) context.getResources().getDimension(R.dimen.fab_margin);
	        int initialRadius, finalRadius;

	        initialRadius = 0;
	        finalRadius = cap.getHeight();

            final Animator animator = ViewAnimationUtils.createCircularReveal(cap,
                    (cap.getWidth() - (fab.getWidth() / 2) - margin),
                    (cap.getHeight() - (fab.getHeight() / 2) - margin),
                    initialRadius, finalRadius);
            animator.start();
        }

	    if (miniFabs != null) {

            for (int i = 0; i < miniFabs.length; i++) {
                final Animation anim = AnimationUtils.loadAnimation(context, R.anim.abc_fade_in);
                anim.setDuration(FADE_DURATION);
                miniFabs[i].setVisibility(View.VISIBLE);
                anim.setStartOffset((i + 1) * OFFSET);
                miniFabs[i].startAnimation(anim);
            }
        }
    }
}
