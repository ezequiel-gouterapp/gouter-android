package security.gouter.Util;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.facebook.Profile;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import security.gouter.GouterApplication;
import security.gouter.Model.User;

/**
 * Created by ezequiel on 18/10/15.
 */
public class UserUtils {

	private static final String ACCESS_UTIL = "A util class can not be instantiated";

    // Local storage constants
    private static final String PROFILE_IMAGES = "profileImages";

	private UserUtils() {
		throw new AssertionError(ACCESS_UTIL);
	}

	public static void saveUserOnpreferences(final Context context,
	                                         final String data) {
		final SharedPreferences prefs = context
				.getSharedPreferences(GouterApplication.PREFERENCES_ACCOUNT,
				Activity.MODE_PRIVATE);
		prefs.edit().putString(GouterApplication.PREFERENCES_USER, data)
				.apply();
	}

	public static User getUserFromPreferences(final Context ctx,
	                                           final Gson gson) {
		final SharedPreferences prefs = ctx.getSharedPreferences(GouterApplication.PREFERENCES_ACCOUNT,
                Activity.MODE_PRIVATE);
		return gson.fromJson(prefs.getString(GouterApplication.PREFERENCES_USER, null),
				User.class);
	}

	public static void resetPreferences(final Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(GouterApplication.PREFERENCES_ACCOUNT,
				Activity.MODE_PRIVATE);
		prefs.edit().remove(GouterApplication.PREFERENCES_USER).commit();
	}

	public static void saveToInternalSorage(final Context ctx, final Bitmap bitmapImage,
	                                          final String userId){
		ContextWrapper cw = new ContextWrapper(ctx);
		// path to /data/data/yourapp/app_data/imageDir
		File directory = cw.getDir(PROFILE_IMAGES, Context.MODE_PRIVATE);
		// Create imageDir
		File path = new File(directory, new StringBuilder().append(
				userId).append(".png").toString());

		FileOutputStream fos = null;
		try {

			fos = new FileOutputStream(path);

			// Use the compress method on the BitMap object to write image to the OutputStream
			bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Bitmap getProfileImageFromStorage(final Context ctx, final String userId)
	{
		try {
			ContextWrapper cw = new ContextWrapper(ctx);
			File directory = cw.getDir(PROFILE_IMAGES, Context.MODE_PRIVATE);
			File f = new File(directory, new StringBuilder().append(
					userId).append(".png").toString());
			Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
			return b;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return null;

	}

    public static String getPorfileImageUri() {
        return Profile.getCurrentProfile().getProfilePictureUri(600, 600).toString();
    }
}
