package security.gouter.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import security.gouter.Model.Event;
import security.gouter.R;

/**
 * Created by ezequiel on 16/11/15.
 */
public class EventDetailInfo extends Fragment {

    private static final String EVENT = "event";

    /**
     * Returns a new instance of EventDetailFragment
     *
     * @param event The event from where it should show information
     * @return The instance
     */
    public static EventDetailInfo newInstance(final Event event) {

        final EventDetailInfo instance = new EventDetailInfo();
        final Bundle bundle = new Bundle();
        bundle.putSerializable(EVENT, event);
        instance.setArguments(bundle);

        return instance;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.event_detail_info, container, false);

        return view;
    }
}
